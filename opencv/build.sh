#!/bin/sh -e
. ../build-vars.sh
IMAGE=opencv
VERSION=4.5.1

docker build . \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg VERSION=$VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/$IMAGE:$VERSION-$ARCH \
  -f ./Dockerfile

if [ "$1" = "push" ]; then
  docker tag $REGISTRY/$IMAGE:$VERSION-$ARCH $REGISTRY/$IMAGE:latest-$ARCH
  docker push $REGISTRY/$IMAGE:$VERSION-$ARCH
  docker push $REGISTRY/$IMAGE:latest-$ARCH
fi
