# Source me!
[ -z "$UBUNTU_ARCH" ] && export UBUNTU_ARCH=arm32v7
[ -z "$UBUNTU_VERSION" ] && export UBUNTU_VERSION=20.04
[ -z "$ARCH" ] && export ARCH=armhf
[ -z "$REGISTRY" ] && export REGISTRY=registry.gitlab.com/unimatrix/containers
[ -z "$TENSORFLOW_VERSION" ] && export TENSORFLOW_VERSION=2.1.0
[ -z "$LAROD_VERSION" ] && export LAROD_VERSION=2.2.61
true
