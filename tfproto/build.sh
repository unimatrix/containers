#!/bin/sh
. ../build-vars.sh
IMAGE=tfproto
VERSION=$TENSORFLOW_VERSION

docker build . \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg TENSORFLOW_VERSION=$VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/$IMAGE:$VERSION-$ARCH \
  -f ./Dockerfile

if [ "$1" = "push" ]; then
  docker tag $REGISTRY/$IMAGE:$VERSION-$ARCH $REGISTRY/$IMAGE:latest-$ARCH
  docker push $REGISTRY/$IMAGE:$VERSION-$ARCH
  docker push $REGISTRY/$IMAGE:latest-$ARCH
fi
