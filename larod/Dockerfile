# Dockerfile to build larod
#
# This Dockerfile creates an environment that can build and run larod.
#
# Example build command:
#     docker build -t builder-larod .
#
# One can then mount the cloned larod directory when running. For example:
#     docker run \
#         --volume /run/dbus:/run/dbus \
#         --cap-add=SYS_PTRACE \
#         --security-opt seccomp=unconfined \
#         -it larod
#
# Further useful options to docker run command:
#   --privileged  Extended privileges, needed for e.g. EdgeTPU on USB.
#   -v /dev:/dev  Mount /dev from host, needed for e.g. EdgeTPU on USB.
#   --gpus all    Used with Nvidia docker to bridge host GPU(s), used with
#                 the GLGPU backend.
# For an explanation of the other options in the example command above, please
# see doc/BUILD.md.

ARG UBUNTU_VERSION=20.04
ARG UBUNTU_ARCH=arm32v7

FROM $UBUNTU_ARCH/ubuntu:$UBUNTU_VERSION as builder-base
ARG SDBUSPLUS_SRC_SHA="847a0c3711fc316e1a9532fc333a0b45a034cb96"
ARG FLATBUFFER_SRC_BR="v1.12.0"
ARG VERSION="2.2.61"
ARG DEBIAN_FRONTEND=noninteractive

LABEL version=$VERSION maintainer="https://gitlab.com/unimatrix/larod"

## Install (larod) dependencies.
RUN apt-get update && apt-get install -y \
    build-essential \
    curl \
    unzip \
    git \
    make \
    pkg-config \
    ninja-build \
    sudo \
    systemd \
    libsystemd-dev \
    libreadline-gplv2-dev \
    python3 \
    python3-pip \
    # sdbusplus dependencies.
    libtool \
    python3-inflection \
    python3-mako \
    python3-yaml \
 && update-alternatives --install /usr/bin/python python /usr/bin/python3 1

RUN python3 -m pip install meson

# Build sdbusplus.
RUN git clone https://github.com/openbmc/sdbusplus.git \
 && cd sdbusplus \
 && git checkout $SDBUSPLUS_SRC_SHA \
 && meson build -Dtests=disabled -Dexamples=disabled -Dprefix=/usr \
 && cd build && ninja \
 && ninja install \
 && cd ../tools \
 && ./setup.py install

# Install old CMake 3.13.4 from Ubuntu 19.10 since newer CMake has a bug,
# see <https://gitlab.kitware.com/cmake/cmake/-/issues/20568>.
RUN echo "deb http://old-releases.ubuntu.com/ubuntu/ eoan main restricted universe multiverse"\
    >> /etc/apt/sources.list \
 && apt-get update \
 && apt-get install -y cmake=3.13.4-1build1 cmake-data=3.13.4-1build1

# Build flatbuffers.
RUN git clone -b $FLATBUFFER_SRC_BR https://github.com/google/flatbuffers.git \
 && cd flatbuffers \
 && cmake cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr CMakeLists.txt \
 && make install \
 && ldconfig

RUN git clone -b R$VERSION https://gitlab.com/unimatrix/larod.git

FROM builder-base AS builder-lib
ARG TARGET_ROOT=/target-root

# Build a minimal Larod.
RUN cd larod \
 && meson build -Dprefix=$TARGET_ROOT/usr -Dstrip=true -Dinstall_client=true \
 && cd build \
 && ninja \
 && ninja install \
 && rm -f $TARGET_ROOT/usr/bin/larod

# Install Larod DBus conf.
RUN install -d $TARGET_ROOT/usr/share/doc/larod \
 && install /larod/src/service/com.axis.Larod1.conf $TARGET_ROOT/usr/share/doc/larod

FROM scratch AS strip-lib
COPY --from=builder-lib /target-root /

FROM builder-base AS builder-larod
ARG LIBYUV_SRC_SHA="b45db3c4af8046f99ababc8ed4181edd2976d2b5"
# Tensorflow lite version 2.3.1
ARG TFLITE_SRC_REV="fcc4b966f1265f466e82617020af93670141b009"
ARG TFLITE_TARGET="rpi"
ARG TFLITE_TARGET_ARCH="armv7l"
ARG LIBGEDGETPU_SRC_REV="ea1eaddbddece0c9ca1166e868f8fd03f4a3199e"
ARG USE_TFLITE_EDGETPU="false"

# Build libyuv.
RUN git clone https://chromium.googlesource.com/libyuv/libyuv \
 && cd libyuv \
 && git checkout $LIBYUV_SRC_SHA \
 && make -f linux.mk \
 && cp -v libyuv.a /usr/lib && cp -vr include /usr

# Clone tensorflow, build and install tensorflow-lite.a.
RUN git clone --depth 1 https://github.com/tensorflow/tensorflow.git \
 && cd tensorflow \
 && git fetch origin $TFLITE_SRC_REV \
 && git reset --hard FETCH_HEAD \
 && ./tensorflow/lite/tools/make/download_dependencies.sh \
 && make -f tensorflow/lite/tools/make/Makefile \
    TARGET=$TFLITE_TARGET TARGET_ARCH=$TFLITE_TARGET_ARCH \
 && find ./tensorflow/lite -name "*.h" | xargs cp --parents -vt /usr/include \
 && find ./tensorflow/lite -name libtensorflow-lite.a | xargs cp -vt /usr/lib

FROM builder-larod AS builder-larod-cpu

# Build larod with tflite and libyuv support.
RUN cd larod \
 && meson build -Dprefix=/usr -Dstrip=true -Dinstall_client=true \
    -Duse_tflite_cpu=true -Duse_libyuv=true \
 && cd build \
 && ninja \
 && ninja install

CMD [ "/usr/bin/larod" ]

FROM builder-larod AS builder-larod-tpu

# Add extra repo to be able get libabsl-dev.
RUN echo "deb http://deb.debian.org/debian bullseye main" \
    >> /etc/apt/sources.list \
 && curl -fsSL https://ftp-master.debian.org/keys/archive-key-10.asc | \
    sudo apt-key add - \
 && curl -fsSL https://ftp-master.debian.org/keys/archive-key-11.asc | \
    sudo apt-key add - \
 && apt-get update

# Build/install libedgetpu from source.
RUN apt-get install -y libabsl-dev xxd libusb-1.0-0-dev \
 && git clone https://github.com/google-coral/libedgetpu.git \
 && cd libedgetpu \
 && git fetch origin $LIBGEDGETPU_SRC_REV \
 && git reset --hard FETCH_HEAD \
 && TFROOT=/tensorflow make -f makefile_build/Makefile -j all libedgetpu \
 && strip out/direct/k8/libedgetpu.so.1.0 \
 && cp out/direct/k8/libedgetpu.so.1.0 /usr/lib \
 && ln -s /usr/lib/libedgetpu.so.1.0 /usr/lib/libedgetpu.so.1 \
 && ln -s /usr/lib/libedgetpu.so.1 /usr/lib/libedgetpu.so \
 && cp tflite/public/edgetpu.h /usr/include

# Build larod with tflite and libyuv support.
RUN cd larod \
 && meson build -Dprefix=/usr -Dstrip=true -Dinstall_client=true \
    -Duse_tflite_cpu=true -Duse_libyuv=true -Duse_tflite_tpu=true \
 && cd build \
 && ninja \
 && ninja install

CMD [ "/usr/bin/larod" ]
