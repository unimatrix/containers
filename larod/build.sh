#!/bin/sh -e
. ../build-vars.sh
IMAGE=larod
VERSION=$LAROD_VERSION

# Enable building for other architectures.
docker run --rm --userns host --privileged multiarch/qemu-user-static:4.2.0-6 --reset -p yes

# Build image with liblarod.
docker build . \
  --target strip-lib \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg VERSION=$VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/liblarod:$VERSION-${ARCH} \
  -f ./Dockerfile

# Build the larod CPU image.
docker build . \
  --target builder-larod-cpu \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg VERSION=$VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/builder-$IMAGE:$VERSION-${ARCH} \
  -f ./Dockerfile

# Build the larod TPU image.
docker build . \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg VERSION=$VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/builder-$IMAGE:$VERSION-${ARCH}-tpu \
  -f ./Dockerfile

# Make a minimal image using docker-slim.
docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock \
  dslim/docker-slim build \
  --target $REGISTRY/builder-$IMAGE:$VERSION-${ARCH} \
  --tag $REGISTRY/$IMAGE:$VERSION-${ARCH} \
  --http-probe=false \
  --continue-after 10 \
  --cmd /usr/bin/larod \
  --mount /run/dbus:/run/dbus

docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock \
  dslim/docker-slim build \
  --target $REGISTRY/builder-$IMAGE:$VERSION-${ARCH}-tpu \
  --tag $REGISTRY/$IMAGE:$VERSION-${ARCH}-tpu \
  --http-probe=false \
  --continue-after 10 \
  --cmd /usr/bin/larod \
  --mount /run/dbus:/run/dbus


if [ "$1" = "push" ]; then
  docker tag $REGISTRY/liblarod:$VERSION-${ARCH} $REGISTRY/liblarod:latest-${ARCH}
  docker push $REGISTRY/liblarod:$VERSION-${ARCH}
  docker push $REGISTRY/liblarod:latest-${ARCH}
  docker tag $REGISTRY/$IMAGE:$VERSION-${ARCH} $REGISTRY/$IMAGE:latest-${ARCH}
  docker push $REGISTRY/$IMAGE:$VERSION-${ARCH}
  docker push $REGISTRY/$IMAGE:latest-${ARCH}
  docker tag $REGISTRY/$IMAGE:$VERSION-${ARCH}-tpu $REGISTRY/$IMAGE:latest-${ARCH}-tpu
  docker push $REGISTRY/$IMAGE:$VERSION-${ARCH}-tpu
  docker push $REGISTRY/$IMAGE:latest-${ARCH}-tpu
fi
