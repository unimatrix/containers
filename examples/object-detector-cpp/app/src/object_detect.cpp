#include <chrono>
#include <fstream>
#include <grpcpp/grpcpp.h>
#include <iomanip>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

#include "serving_client.hpp"
#include "test_certificate.h"

//#define USE_SSL

using namespace std;
using namespace cv;
using namespace chrono;
using namespace grpc;

const float CONFIDENCE_CUTOFF = 0.3;

vector<string> get_classes_from_file() {
  string str;
  vector<string> classes;
  ifstream file(getenv("OBJECT_LIST_PATH"));

  if (!file) {
    cerr << "Cannot open classes file" << endl;
    exit(EXIT_FAILURE);
  }

  getline(file, str); // Ignore the first line
  while (getline(file, str)) {
    if (str.size() > 0)
      classes.push_back(str);
  }

  return classes;
}

VideoCapture setup_capture(int nbr_buffers) {
  // Open a video capture from source with index 0
  VideoCapture cap(0);

  // Validate all properties by trying to grab one frame
  try {
    cap.grab();
  } catch (const std::exception &e) {
    cerr << "Failed to open stream: " << e.what() << endl;
    exit(EXIT_FAILURE);
  }

  return cap;
}

void preprocess(cv::Mat &image, cv::Size inpSize) {
  float scale_x = float(inpSize.width) / image.cols;
  float scale_y = float(inpSize.height) / image.rows;
  float scale = min(scale_x, scale_y);

  if (scale != 1.0) {
    cv::resize(image, image, Size(), scale, scale);
  }

  int padding_x = max(inpSize.width - image.cols, 0);
  int padding_y = max(inpSize.height - image.rows, 0);

  if (padding_x != 0 || padding_y != 0) {
    cv::copyMakeBorder(image, image, padding_y, 0, padding_x, 0,
                       BORDER_CONSTANT, Scalar(0, 0, 0));
  }
}

void postprocess(PredictResponse &response, const vector<string> &classes) {
  OutMap &outputs = *response.mutable_outputs();

  const float *boxes =
      (const float *)outputs["output0"].tensor_content().data();
  const float *objects =
      (const float *)outputs["output1"].tensor_content().data();
  const float *confidences =
      (const float *)outputs["output2"].tensor_content().data();
  const int nelm = 50;

  cout << fixed << setprecision(2);

  for (int i = 0; i < nelm; i++) {
    const string object = classes[round(objects[i])];
    const float &confidence = confidences[i];
    const float *pbox = boxes + 4 * i;

    // Stop when confidence is too low
    if (confidence <= CONFIDENCE_CUTOFF || confidence > 1.0) {
      break;
    }

    cout << "Object: " << object;
    cout << ", Confidence: " << confidence;
    cout << ", Box: [" << pbox[0] << ", " << pbox[1] << ", " << pbox[2] << ", "
         << pbox[3] << "]" << endl;
  }
}

inline int64_t ms_time(steady_clock::time_point start,
                       steady_clock::time_point end) {
  return duration_cast<milliseconds>(end - start).count();
}

void output_timing_info(steady_clock::time_point start,
                        steady_clock::time_point framecapture_end,
                        steady_clock::time_point preprocess_end,
                        steady_clock::time_point grpc_end,
                        steady_clock::time_point postprocess_end) {

  cout << "Capture: " << ms_time(start, framecapture_end) << " ms"
       << endl;
  cout << "Preprocess: " << ms_time(framecapture_end, preprocess_end) << " ms"
       << endl;
  cout << "Inference grpc call: " << ms_time(preprocess_end, grpc_end) << " ms"
       << endl;
  cout << "Postprocess: " << ms_time(grpc_end, postprocess_end) << " ms"
       << endl;
}

int main(int argc, char *argv[]) try {
  // Number of outstanding zero-copy buffers (This is a very precious resource)
  constexpr size_t BUFFERS = 2;
  Mat frame[BUFFERS];

  cout << "Start: " << argv[0] << endl;
  vector<string> classes = get_classes_from_file();
  VideoCapture cap = setup_capture(BUFFERS);
  string connect_string = string(getenv("INFERENCE_HOST")) + string(":") +
                          string(getenv("INFERENCE_PORT"));

#ifdef USE_SSL
  SslCredentialsOptions ssl_opts = {test_certificate, "", ""};
  shared_ptr<ChannelCredentials> creds = grpc::SslCredentials(ssl_opts);
  grpc::ChannelArguments args;
  args.SetSslTargetNameOverride("localhost");
  ServingClient client(grpc::CreateCustomChannel(connect_string, creds, args));
#else
  shared_ptr<ChannelCredentials> creds = InsecureChannelCredentials();
  ServingClient client(grpc::CreateChannel(connect_string, creds));
#endif

  string model(getenv("MODEL_PATH"));

  const string s(getenv("MODEL_INPUT_SIZE"));
  size_t delim_pos = s.find("x");
  int width = std::stoi(s.substr(0, delim_pos));
  int height = std::stoi(s.substr(delim_pos + 1, std::string::npos));
  cv::Size model_input_size(width, height);

  int frame_idx = 0;
  for (;;) {
    Mat &mat = frame[frame_idx++ % BUFFERS];

    auto time_start = steady_clock::now();

    // Blocking read
    cap.read(mat);

    auto time_framecapture_end = steady_clock::now();

    // Make sure capture succeeded
    if (mat.empty())
      throw std::runtime_error("Failed to fetch frame");

    uint32_t seqnum = cap.get(CAP_PROP_POS_FRAMES);
    // Write info about frame
    cout << "Caught frame " << setw(2) << seqnum << " " << mat.cols << "x"
         << mat.rows << endl;

    // Preprocess image
    preprocess(mat, model_input_size);

    auto time_preprocess_end = steady_clock::now();

    cout << "Connecting to: " << connect_string << endl;

    string output;
    // Call the inference server
    auto maybe_response = client.callPredict(model, mat, output);

    auto time_grpc_end = steady_clock::now();

    cout << output << endl;

    // Run postprocessing if we got a valid result
    if (maybe_response) {
      auto response = maybe_response.value();
      postprocess(response, classes);
    }

    auto time_postprocess_end = steady_clock::now();

    output_timing_info(time_start, time_framecapture_end, time_preprocess_end,
                       time_grpc_end, time_postprocess_end);

    cout << endl;
  }

  return 0;

} catch (const exception &e) {
  cerr << "Exception: " << e.what() << endl;
}
