# An object detection application in C++ using larod-inference-server

The example code is written in C++ for object detection using larod-inference-server. The example uses the following technologies:

* OpenCV
* larod-inference-server
* ssdlite-mobilenet-v2

## Overview

This example composes three different container images into an application that performs object detection using a deep learning model.

The first container contains the actual program being built here, which uses OpenCV to capture pictures from the camera and modifies them to fit the input required by the model. It then uses grpc/protobuf to call the second container, the "larod-inference-server", that performs the actual inference. The inference server implements the TensorFlow Serving API.

Lastly, there is a third container that holds the deep learning model, which is put into a volume that is accessible to the other two images.

## Example Structure

Below is the structure of the example with a brief description of scripts.

<pre>
object-detector-cpp
 |- app
 | |- src
 | | |- object_detect.cpp - App to capture the video stream using OpenCV in C++ to detect objects.
 | | |- serving_client.hpp - Creates the request and makes the call to the inference-server.
 | |- Dockerfile - Builds the program using an image that contains everything necessary for building, and copies it into another image.
 | |- Makefile - Used by the make tool to build the program
 |- README.md - How to execute the example
 |- docker-compose.yml - Specifies the group of images used to run the application, and their interdependencies.
 |- build.sh - A convenience script that builds the app.
</pre>

## Prerequisites

## Running the example code

1. In the `app/` directory, build the object-detector-cpp image:

```sh
docker build --tag object-detector-cpp --build-arg http_proxy=<optional_proxy_URL> .  
```

2. Go back to the repository root folder to start the service through docker-compose. Settings can be configured in the docker-compose.yml file.

```sh
docker-compose up
```

## Zero copy

This example uses larod-inference-server for video inference processing by using gRPC API. In case this client and the inference server is located on the same device, it is possible to speed up inference by using shared memory to pass the video image to the inference server by activating following define statement in file src/serving_client.hpp:

```c++
#define ZEROCOPY
```

## Server Authentication

This example uses larod-inference-server for video inference processing. The API uses an insecure gRPC communication channel, but it is possible to activate SSL/TLS server authentication and encryption by activating following define statement in file src/object_detect.cpp:

```c++
#define USE_SSL
```

When SSL/TLS is activated, a certificate and private key for your organization must be provided to the inference server. Here is an example how to generate a temporary test certificate:

```sh
# Generate TSL/SSL test certificate
# Press default for all input except: Common Name (e.g. server FQDN or YOUR name) []:localhost
 openssl req -x509 -newkey rsa:4096 -nodes -days 365 -out testdata/server.pem -keyout testdata/server.key
```

The inference server must be started by specifying the certificate and the private key in the file docker-compose.yml:

```sh
/usr/bin/larod-inference-server -c certificate.pem -k private.key
```

## Model over gRPC

This example uses larod-inference-server for video inference processing by using gRPC API. The inference server supports multiple clients at the same time. Models are normally loaded when the inference server is starting up, but models can also be loaded by specifying the model file path over gRPC. Please note the model path specified must be accessible by the inference server.

## License

**Apache License 2.0**
