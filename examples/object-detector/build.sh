#!/bin/sh -e
. ../../build-vars.sh
VERSION=2.1

docker build app \
  --platform $ARCH \
  -t $REGISTRY/examples/object-detector:$VERSION-$ARCH \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg REGISTRY=$REGISTRY \
  --build-arg ARCH=$ARCH \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -f app/Dockerfile

if [ "$1" = "push" ]; then
  docker tag $REGISTRY/examples/object-detector:$VERSION-$ARCH $REGISTRY/examples/object-detector:latest-$ARCH
  docker push $REGISTRY/examples/object-detector:$VERSION-$ARCH
  docker push $REGISTRY/examples/object-detector:latest-$ARCH
fi
