# Object detector example

## Run

```bash
docker-compose up
```

To run on a system with the Coral edge TPU USB stick:

```bash
docker-compose -f docker-compose-tpu.yml up
```

## View

The application starts a web server on port [5000](<http://localhost:5000>) and streams images continuously.
