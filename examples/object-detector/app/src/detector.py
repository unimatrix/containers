import cv2
import logging
import numpy as np
import os
import time

from tf_proto_utils import InferenceClient
from cv_streamer import imshow

logger = logging.getLogger(__name__)

def rescale(image, width, height, maintain_ratio=True):
    if not maintain_ratio:
        return cv2.resize(image, (width, height))
    org_height, org_width = image.shape[:2]
    width_ratio = float(width) / org_width
    height_ratio = float(height) / org_height
    if width_ratio < height_ratio:
        tmp_width = width
        tmp_height = int(org_height * width_ratio)
        bottom, right = height - tmp_height, 0
        width_mul, height_mul = 1, float(height) / tmp_height
    elif width_ratio > height_ratio:
        tmp_width = int(org_width * height_ratio)
        tmp_height = height
        bottom, right = 0, width - tmp_width
        width_mul, height_mul = float(width) / tmp_width, 1
    else:
        return cv2.resize(image, (width, height)), 1, 1
    image = cv2.resize(image, (tmp_width, tmp_height))
    return cv2.copyMakeBorder(image, 0, bottom, 0, right, cv2.BORDER_CONSTANT, value=(0, 0, 0)), width_mul, height_mul

class Detector:
    def __init__(self):
        self.video_source = None
        self.detection_type = None
        self.inference_client = None
        self.model_input_height = None
        self.model_input_width = None
        self.model_name = None
        self.object_list = None
        self.threshold = None

    def detect(self, image):
        image, width_mul, height_mul = rescale(image, self.model_input_width, self.model_input_height)
        image = np.expand_dims(image, axis=0)
        image = image.astype(np.uint8)
        success, result = self.inference_client.infer({'data': image}, self.model_name)
        logger.debug('Inference consumed {} ms'.format(self.inference_client.infer_time))
        if not success:
            return False, 0, 0, 0
        bounding_boxes, classes, confidences = tuple([np.squeeze(result[key]) for key in ['output0', 'output1', 'output2']])

        for i, confidence in enumerate(confidences):
            if confidence < self.threshold:
                logger.info('{} {} found'.format(i, self.detection_type))
                return True, bounding_boxes[:i]*np.array([height_mul, width_mul, height_mul, width_mul]), classes[:i], confidences[:i]
        return True, bounding_boxes, classes, confidences

    def read_object_list(self, object_list_path):
        self.object_list = None
        self.detection_type = 'Objects'
        if object_list_path is not None:
            object_list_file = open(object_list_path, 'r')
            self.object_list = object_list_file.read().splitlines()
            self.detection_type = self.object_list[0]

    def draw_bounding_boxes(self, image, bounding_boxes, obj_classes, color=(1, 190, 200)):
        height, width = image.shape[:2]
        for bounding_box, obj_class in zip(bounding_boxes, obj_classes):
            cv2.rectangle(image, (int(bounding_box[1]*width), int(bounding_box[0]*height)), (int(bounding_box[3]*width), int(bounding_box[2]*height)), color, 2)
            if self.object_list is not None:
                cv2.putText(image, self.object_list[int(obj_class.item()) + 1], (int(bounding_box[1]*width), int(bounding_box[0]*height-10)), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)
                logger.info(self.object_list[int(obj_class.item()) + 1])
        return image

    def init(self):
        level_config = {'debug': logging.DEBUG, 'info': logging.INFO, 'warning': logging.WARNING, "error": logging.ERROR }
        logging_level = logging.WARNING
        if os.environ.get('LOG_LEVEL') is not None:
            logging_level = level_config[os.environ.get('LOG_LEVEL').lower()]
        logging.basicConfig(format='%(message)s', level=logging_level)
        self.video_source = int(os.environ.get('VIDEO_SOURCE'))
        self.threshold = float(os.environ.get('DETECTION_THRESHOLD', 0.5))
        self.inference_client = InferenceClient(os.environ['INFERENCE_HOST'], int(os.environ['INFERENCE_PORT']))
        self.model_name = os.environ['MODEL_NAME']
        self.model_input_width, self.model_input_height = [int(i) for i in os.environ['MODEL_INPUT_SIZE'].split('x')]
        image_path = os.environ.get('IMAGE_PATH')
        object_list_path = os.environ.get('OBJECT_LIST_PATH')
        return image_path, object_list_path

    def run(self):
        image_path, object_list_path = self.init()
        self.read_object_list(object_list_path)
        if image_path is not None:
            self.run_image_source(image_path)
        else:
            self.run_camera_source()

    def run_camera_source(self):
        cap = cv2.VideoCapture(self.video_source)
        while True:
            start_time = time.time()
            _, frame = cap.read()
            capture_time = int((time.time() - start_time)*1000)
            logger.debug('Frame capture consumed {} ms'.format(capture_time))
            succeed, bounding_boxes, obj_classes, _ = self.detect(frame)
            if not succeed:
                time.sleep(1)
                continue
            frame = self.draw_bounding_boxes(frame, bounding_boxes, obj_classes)
            fps = round(1.0 / (time.time() - start_time), 2)
            logger.info('FPS: {}'.format(fps))
            imshow(frame)

    def run_image_source(self, image_path):
        image = cv2.imread(image_path)
        succeed = False
        while not succeed:
            succeed, bounding_boxes, obj_classes, _ = self.detect(image)
            time.sleep(1)
        image = self.draw_bounding_boxes(image, bounding_boxes, obj_classes)
        imshow(image)

if __name__ == '__main__':
    Detector().run()
