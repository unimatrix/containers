"""
 * Copyright (C) 2021 Axis Communications AB, Lund, Sweden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
"""

from flask import Flask, render_template, Response
import threading
import cv2

event = threading.Event()
buffer = None


def imshow(frame):
    global event, buffer
    ret, buffer = cv2.imencode('.jpg', frame)
    if (ret):
        event.set()


def get_frame():
    global event, buffer
    event.wait()
    return buffer


# Setup Flask web server
flask = Flask(__name__)

# Video streaming route. Put this in the src attribute of an img tag
@flask.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


# Index page route
@flask.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


# Generate frame by frame from camera
def gen_frames():
    while True:
        frame = get_frame()
        bytes = frame.tobytes()
        yield (b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + bytes + b'\r\n')


# Preparing parameters for flask to be given in the thread
# so that it doesn't collide with main thread
kwargs = {'host': '0.0.0.0', 'port': 5000, 'threaded': True, 'use_reloader': False, 'debug': False }

# Running flask thread
flaskThread = threading.Thread(target = flask.run, daemon = True, kwargs = kwargs)
flaskThread.start()
