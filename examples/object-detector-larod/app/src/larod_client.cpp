#include <fstream>
#include <iomanip>
#include <iostream>

#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "larod_client.hpp"

using namespace std;


void* createAndMapTmpFile(size_t fileSize, void* addr, int* convFd)
{
  char fileName[] = "/tmp/LarodClient-XXXXXX";
  int fd = mkstemp(fileName);
  if (fd < 0) {
    cerr << "Unable to open temp file: " << fileName << ": " << strerror(errno) << endl;
    return NULL;
  }

  // Allocate enough space in for the fd.
  if (ftruncate(fd, (off_t) fileSize) < 0) {
    cerr << "Unable to truncate temp file " << fileName << ": " << strerror(errno) << endl;
    close(fd);
    return NULL;
  }

  // Remove since we don't actually care about writing to the file system.
  if (unlink(fileName)) {
    cerr << "Unable to unlink temp file" << fileName << ": " << strerror(errno) << endl;
    close(fd);
    return NULL;
  }

  // Get an address to fd's memory for this process's memory space.
  void* data = mmap(addr, fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED) {
    cerr << "Unable to mmap temp file " <<  fileName << ": " << strerror(errno) << endl;
    close(fd);
    return NULL;
  }

  *convFd = fd;

  return data;
}

LarodClient::LarodClient()
  : conn(NULL),
    model(NULL),
    numInputs(0),
    numOutputs(0),
    inputTensors(NULL),
    outputTensors(NULL),
    inputSizes(NULL),
    outputSizes(NULL),
    inputData(NULL),
    outputData(NULL),
    job(NULL)
{
  larodError* error = NULL;
  if (!larodConnect(&conn, &error)) {
    cerr << "Failed to connect to larod: " << error->msg << endl;
    larodClearError(&error);
    throw std::runtime_error("Failed to connect to larod");
  }
  cout << "Connected to Larod" << endl;
}

LarodClient::~LarodClient()
{
  this->destroyModel();
  larodError* error = NULL;
  if (!larodDisconnect(&conn, &error)) {
    cerr << "Failed to disconnect from larod: " << error->msg << endl;
    larodClearError(&error);
  }
}

bool LarodClient::loadModel(const char* modelFile, larodChip chip)
{
  if (model) {
    cerr << "Model already loaded" << endl;
    return false;
  }

  cout << "Loading model " << modelFile << " for chip type #" << chip << endl;
  FILE* fpModel = fopen(modelFile, "rb");
  if (!fpModel) {
    cerr << "Could not open model file " << modelFile << ": " << strerror(errno) << endl;
    return false;
  }

  larodError* error = NULL;
  model  = larodLoadModel(conn, 
                          fileno(fpModel),
                          chip,
                          LAROD_ACCESS_PUBLIC,
                          modelFile,
                          NULL,
                          &error);
  if (!model) {
    cerr << "Could not load model: " << error->msg << endl;
    larodClearError(&error);
    fclose(fpModel);
    return false;
  }

  fclose(fpModel);

  cout << "Model loaded" << endl;

  return this->createJob();
}

void LarodClient::destroyModel()
{
  this->destroyJob();
  larodDestroyModel(&model);
}

bool LarodClient::createJob()
{
  larodError* error = NULL;

  cout << "Creating input and output tensors" << endl;
  inputTensors = larodCreateModelInputs(model, &numInputs, &error);
  if (!inputTensors) {
    cerr << "Failed creating input tensors: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }
  if (numInputs != 1) {
    cerr << "Model has " << numInputs << " inputs, client only supports 1 input tensor." << endl;
    return false;
  }

  inputSizes = larodGetModelInputByteSizes(model, &numInputs, &error);
  if (!inputSizes) {
    cerr << "Failed to get input tensor sizes: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }

  // Map a temporary file for input data.
  int inputFd = -1;
  inputData = createAndMapTmpFile(inputSizes[0], NULL, &inputFd);
  if (!inputData) {
    return false;
  }

  // Set the input data fd.
  if (!larodSetTensorFd(inputTensors[0], inputFd, &error)) {
    cerr << "Failed setting input tensor fd: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }

  outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
  if (!outputTensors) {
    cerr << "Failed creating output tensors: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }

  outputSizes = larodGetModelOutputByteSizes(model, &numOutputs, &error);
  if (!outputSizes) {
    cerr << "Failed to get output tensor sizes: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }

  // Map output tensor data files.
  int* outputFd = new int[numOutputs];
  outputData = new void*[numOutputs];
  for (size_t i = 0; i < numOutputs; i++) {
    cout << "Output #" << i << " size: " << outputSizes[i] << endl;
    outputData[i] = createAndMapTmpFile(outputSizes[i], NULL, &outputFd[i]);
    if (!outputData[i]) {
      return false;
    }

    if (!larodSetTensorFd(outputTensors[i], outputFd[i], &error)) {
      cerr << "Failed setting output tensor fd: " << error->msg << endl;
      larodClearError(&error);
      return false;
    }
  }

  // Create the job request.
  job = larodCreateJobRequest(model,
                              inputTensors,
                              numInputs,
                              outputTensors,
                              numOutputs,
                              NULL,
                              &error);
  if (!job) {
    cerr << "Failed creating job request: " << error->msg << endl;
    larodClearError(&error);
    return false;
  }

  return true;
}

void LarodClient::destroyJob()
{
  if (inputTensors && inputSizes) {
    int fd = larodGetTensorFd(inputTensors[0], NULL);
    if (fd > 0) {
      close(fd);
    }
    if (munmap(inputData, inputSizes[0]) < 0) {
      cerr << "Unmap input data failed: " << strerror(errno) << endl;
    }
    free(inputSizes);
    inputSizes = NULL;
  }

  if (outputTensors && outputSizes) {
    for (size_t i = 0; i < numOutputs; i++) {
      int fd = larodGetTensorFd(outputTensors[0], NULL);
      if (fd > 0) {
        close(fd);
      }
      if (outputSizes) {
        if (munmap(outputData[i], outputSizes[i]) < 0) {
          cerr << "Unmap output data failed: " << strerror(errno) << endl;
        }
      }
    }
    free(outputSizes);
    outputSizes = NULL;
  }

  inputData = NULL;
  outputData = NULL;

  larodDestroyTensors(&inputTensors, numInputs);
  numInputs = 0;
  larodDestroyTensors(&outputTensors, numOutputs);
  numOutputs = 0;

  larodDestroyJobRequest(&job);
}

bool LarodClient::runJob(cv::Mat &image)
{
  bool ret = true;
  memcpy(inputData, image.data, inputSizes[0]);
  larodError* error = NULL;
  if (!larodRunJob(conn, job, &error)) {
    cerr << "Unable to run inference job: " << error->msg << endl;
    larodClearError(&error);
    ret = false;
  }
  return ret;
}

void LarodClient::postprocess(float cutOff, const vector<string> &classes)
{
  const float *boxes = (const float *)outputData[0];
  const float *objects = (const float *)outputData[1];
  const float *confidences = (const float *)outputData[2];
  unsigned numElements = unsigned(*(const float *)outputData[3]);

  cout << fixed << setprecision(2);

  for (unsigned i = 0; i < numElements; i++) {
    const string object = classes[round(objects[i])];
    const float &confidence = confidences[i];
    const float *pbox = boxes + 4 * i;

    // Stop when confidence is too low
    if (confidence <= cutOff || confidence > 1.0) {
      break;
    }

    cout << "Object: " << object;
    cout << ", Confidence: " << confidence;
    cout << ", Box: [" << pbox[0] << ", " << pbox[1] << ", " << pbox[2] << ", "
         << pbox[3] << "]" << endl;
  }
}


bool LarodClient::resetFds()
{
  bool ret = true;
  larodError* error = NULL;

  if (!larodSetTensorFdOffset(inputTensors[0], 0, &error)) {
    cerr << "Unable to reset input tensor fd : " << error->msg << endl;
    larodClearError(&error);
    ret = false;
  }

  for (size_t i = 0; i < numOutputs; i++) {
    if (!larodSetTensorFdOffset(outputTensors[i], 0, &error)) {
      cerr << "Unable to reset output tensor fd #" << i << ": " << error->msg << endl;
      larodClearError(&error);
      ret = false;
    }
  }

  return ret;
}
