#include <larod.h>
#include <opencv2/core.hpp>

void* createAndMapTmpFile(size_t fileSize, void* addr, int* convFd);

class LarodClient {
public:
  LarodClient();
  ~LarodClient();

  bool loadModel(const char* modelFile, larodChip chip);
  void destroyModel();
  bool createJob();
  void destroyJob();
  bool runJob(cv::Mat &image);
  void postprocess(float cutOff, const std::vector<std::string> &classes);
  bool resetFds();

private:
  larodConnection* conn;
  larodModel* model;
  size_t numInputs;
  size_t numOutputs;
  larodTensor** inputTensors;
  larodTensor** outputTensors;
  size_t* inputSizes;
  size_t* outputSizes;
  void* inputData;
  void** outputData;
  larodJobRequest* job;
};
