#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

#include "larod_client.hpp"

using namespace std;
using namespace cv;
using namespace chrono;

const unsigned IMG_WIDTH = 480;
const unsigned IMG_HEIGHT = 320;
const float CONFIDENCE_CUTOFF = 0.3;

vector<string> get_classes_from_file() {
  string str;
  vector<string> classes;
  ifstream file(getenv("OBJECT_LIST_PATH"));

  if (!file) {
    cerr << "Cannot open classes file" << endl;
    exit(EXIT_FAILURE);
  }

  getline(file, str); // Ignore the first line
  while (getline(file, str)) {
    if (str.size() > 0)
      classes.push_back(str);
  }

  return classes;
}

VideoCapture setup_capture(int index) {
  // Open a video capture from supplied index.
  VideoCapture cap(index);
  cap.set(CAP_PROP_FRAME_WIDTH, IMG_WIDTH);
  cap.set(CAP_PROP_FRAME_HEIGHT, IMG_HEIGHT);
  cap.set(CAP_PROP_FOURCC, VideoWriter::fourcc('R', 'G', 'B', '3'));

  // Validate all properties by trying to grab one frame
  try {
    cap.grab();
  } catch (const std::exception &e) {
    cerr << "Failed to open stream: " << e.what() << endl;
    exit(EXIT_FAILURE);
  }

  return cap;
}

void preprocess(cv::Mat &image, cv::Size inpSize) {
  float scale_x = float(inpSize.width) / image.cols;
  float scale_y = float(inpSize.height) / image.rows;
  float scale = min(scale_x, scale_y);

  if (scale != 1.0) {
    cv::resize(image, image, Size(), scale, scale);
  }

  int padding_x = max(inpSize.width - image.cols, 0);
  int padding_y = max(inpSize.height - image.rows, 0);

  if (padding_x != 0 || padding_y != 0) {
    cv::copyMakeBorder(image, image, padding_y, 0, padding_x, 0,
                       BORDER_CONSTANT, Scalar(0, 0, 0));
  }
}

inline int64_t ms_time(steady_clock::time_point start,
                       steady_clock::time_point end) {
  return duration_cast<milliseconds>(end - start).count();
}

void output_timing_info(steady_clock::time_point start,
                        steady_clock::time_point framecapture_end,
                        steady_clock::time_point preprocess_end,
                        steady_clock::time_point inference_end,
                        steady_clock::time_point postprocess_end) {

  cout << "Capture: " << ms_time(start, framecapture_end) << " ms"
       << endl;
  cout << "Preprocess: " << ms_time(framecapture_end, preprocess_end) << " ms"
       << endl;
  cout << "Inference: " << ms_time(preprocess_end, inference_end) << " ms"
       << endl;
  cout << "Postprocess: " << ms_time(inference_end, postprocess_end) << " ms"
       << endl;
}

int main(int argc, char *argv[]) try {
  // Number of outstanding zero-copy buffers (This is a very precious resource)
  constexpr size_t BUFFERS = 2;
  Mat frame[BUFFERS];

  cout << "Start: " << argv[0] << endl;
  vector<string> classes = get_classes_from_file();
  VideoCapture cap = setup_capture(atoi(getenv("VIDEO_SOURCE")));

  const string s(getenv("MODEL_INPUT_SIZE"));
  size_t delim_pos = s.find("x");
  int width = std::stoi(s.substr(0, delim_pos));
  int height = std::stoi(s.substr(delim_pos + 1, std::string::npos));

  string modelName(getenv("MODEL_PATH"));
  larodChip chip = (modelName.find("-tpu") == string::npos ?
                    LAROD_CHIP_TFLITE_CPU : LAROD_CHIP_TPU);
  LarodClient client;
  if (!client.loadModel(modelName.data(), chip)) {
    exit(EXIT_FAILURE);
  }

  int frame_idx = 0;
  for (;;) {
    Mat &mat = frame[frame_idx++ % BUFFERS];

    auto time_start = steady_clock::now();

    // Blocking read
    cap.read(mat);

    auto time_framecapture_end = steady_clock::now();

    // Make sure capture succeeded
    if (mat.empty()) {
      throw std::runtime_error("Failed to fetch frame");
    }

    uint32_t seqnum = cap.get(CAP_PROP_POS_FRAMES);
    // Write info about frame
    cout << "Caught frame " << setw(2) << seqnum << " " << mat.cols << "x"
         << mat.rows << endl;

    // Preprocess image
    preprocess(mat, cv::Size(width, height));
    auto time_preprocess_end = steady_clock::now();

    // Inference call.
    client.runJob(mat);
    auto time_inference_end = steady_clock::now();

    client.postprocess(CONFIDENCE_CUTOFF, classes);
    auto time_postprocess_end = steady_clock::now();

    output_timing_info(time_start, time_framecapture_end, time_preprocess_end,
                       time_inference_end, time_postprocess_end);

    // Since we are reusing input/output buffers for the next request,
    // we should rewind the file positions before next inference job.
    client.resetFds();

    cout << endl;
  }

  return 0;

} catch (const exception &e) {
  cerr << "Exception: " << e.what() << endl;
}
