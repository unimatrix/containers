#!/bin/sh -e
. ../../build-vars.sh
IMAGE=examples/object-detector-larod
VERSION=2.0

docker build app \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg REGISTRY=$REGISTRY \
  --build-arg VERSION=$VERSION \
  --build-arg ARCH=$ARCH \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/$IMAGE:$VERSION-$ARCH \
  -f app/Dockerfile

if [ "$1" = "push" ]; then
  docker tag $REGISTRY/$IMAGE:$VERSION-$ARCH $REGISTRY/$IMAGE:latest-$ARCH
  docker push $REGISTRY/$IMAGE:$VERSION-$ARCH
  docker push $REGISTRY/$IMAGE:latest-$ARCH
fi
