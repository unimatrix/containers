ARG UBUNTU_ARCH=arm32v7
ARG UBUNTU_VERSION=20.04
ARG REGISTRY=registry.gitlab.com/unimatrix/containers
ARG ARCH=armhf
ARG LAROD_VERSION=2.2.61
ARG TENSORFLOW_VERSION=2.1.0

FROM $REGISTRY/tfproto:${TENSORFLOW_VERSION}-${ARCH} AS tf-proto
FROM $REGISTRY/liblarod:${LAROD_VERSION}-${ARCH} AS liblarod
FROM $UBUNTU_ARCH/ubuntu:$UBUNTU_VERSION AS builder

ARG DEBIAN_FRONTEND=noninteractive
ARG TENSORFLOW_VERSION

## Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    git \
    make \
    pkg-config \
    libc++-dev \
    libgrpc++-dev \
    libprotobuf-dev \
    libc-ares-dev \
    libssl-dev \
    libgtest-dev \
    libgflags-dev \
    protobuf-compiler \
    protobuf-compiler-grpc

# Get Tensorflow
RUN git clone --depth=1 -b v$TENSORFLOW_VERSION https://github.com/tensorflow/tensorflow.git /opt/tensorflow/tensorflow

# Get Tensorflow Serving
RUN git clone --depth=1 -b $TENSORFLOW_VERSION https://github.com/tensorflow/serving.git /opt/tensorflow/serving
WORKDIR /opt/larod-inference-server
RUN mkdir apis \
 && cd apis \
 && ln -fs /opt/tensorflow/tensorflow/tensorflow \
 && ln -fs /opt/tensorflow/serving/tensorflow_serving

## Copy source files
COPY larod-inference-server ./

# Get TensorFlow protos.
COPY --from=tf-proto /tf-proto /axis/tf-proto

# Get Larod lib.
COPY --from=liblarod / /

##  Build and install
RUN make install && ldconfig

WORKDIR /model
EXPOSE 8501
CMD [ "/usr/bin/larod-inference-server", "-p", "8501" ]
