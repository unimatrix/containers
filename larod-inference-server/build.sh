#!/bin/sh -e
. ../build-vars.sh
IMAGE=larod-inference-server
VERSION=2.2.2

# Enable building for other architectures.
docker run --rm --userns host --privileged multiarch/qemu-user-static:4.2.0-6 --reset -p yes

if [ ! -d larod-inference-server ]; then
  git clone -b $VERSION https://gitlab.com/unimatrix/larod-inference-server.git
fi

# Build the builder image.
docker build . \
  --build-arg UBUNTU_ARCH=$UBUNTU_ARCH \
  --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
  --build-arg ARCH=$ARCH \
  --build-arg REGISTRY=$REGISTRY \
  --build-arg LAROD_VERSION=$LAROD_VERSION \
  --build-arg TENSORFLOW_VERSION=$TENSORFLOW_VERSION \
  --build-arg http_proxy=$http_proxy \
  --build-arg https_proxy=$https_proxy \
  -t $REGISTRY/builder-$IMAGE:$VERSION-$ARCH \
  -f ./Dockerfile

# Make a minimal image using docker-slim.
docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock \
  dslim/docker-slim build \
  --target $REGISTRY/builder-$IMAGE:$VERSION-${ARCH} \
  --tag $REGISTRY/$IMAGE:$VERSION-$ARCH \
  --http-probe=false \
  --continue-after 10 \
  --cmd /usr/bin/larod-inference-server \
  --mount /run/dbus:/run/dbus

if [ "$1" = "push" ]; then
  docker tag $REGISTRY/$IMAGE:$VERSION-$ARCH $REGISTRY/$IMAGE:latest-$ARCH
  docker push $REGISTRY/$IMAGE:$VERSION-$ARCH
  docker push $REGISTRY/$IMAGE:latest-$ARCH
fi
